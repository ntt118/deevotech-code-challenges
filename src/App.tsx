import React, { useEffect, useState } from 'react';
import './App.css';

function App() {

  const [data, setData] = useState([])
  const [textSearch, setTextSearch] = useState("")
  const [language, setLanguage] = useState("english")

  useEffect(() => {
    searchData()
  }, [])

  const searchData = () => {
    fetch(`https://www.thecocktaildb.com/api/json/v1/1/search.php?s=${textSearch}`)
    .then(response => response.json())
    .then(data => setData(data ? data.drinks : []));
  }

  const onChangeLanguage = (lang: any) => {
    setLanguage(lang)
  }

  const getIntrustion = (item: any) => {
    if(language === 'germany'){
      return item.strInstructions ?? "Nicht verfügbar"
    } else if (language === 'italy'){
      return item.strInstructionsDE ?? "Non disponibile"
    } else {
      return item.strInstructionsIT ?? "Not available"
    }
  }

  return (
    <div className="App">
      <div className="header-search">
        <div className="header-search__title">Search CockTail by name</div>
        <div className="header-search__input-search">
          <input className="header-search__input" type="text" onKeyUp={(e) => {if(e.keyCode === 13) searchData()} } onChange={(e) => setTextSearch(e.target.value)} />
          <button className="header-search__button" placeholder="Nhập tên cocktail..." onClick={() => searchData()}>Search</button>
          <select className="header-search__select" name="language" onChange={(e) => onChangeLanguage(e.target.value)}> Select Language
            <option value="enlish">English</option>
            <option value="germany">Germany</option>
            <option value="italy">Italy</option>
          </select>
        </div>
      </div>
      <div className="app-body">
        {data ? data.length > 0 && data.map((rs: any) => <div key={rs.idDrink} className="app-body__item">
          <img className="app-body__item--image" src={rs.strDrinkThumb} alt={rs.strDrink} />
          <div className="app-body__item--des">{getIntrustion(rs)}</div>
        </div>)
        : <div className="no-item">No product</div>
        }
      </div>
    </div>
  );
}

export default App;
